---
layout: ../layouts/AboutLayout.astro
title: ""
---

AstroPaper 是一个小的响应迅速的，对 SEO 友好的博客。[作者](https://satnaing.dev/blog)根据自己的博客设计和制作了这个主题。

此主题旨在开箱即用。浅色和深色模式由 还可以配置默认配色方案和其他配色方案。

这个主题中的文章、帖子也可以被视为文档。因此，请参阅文档以获取更多信息。

<div>
  <img src="/assets/dev.svg" class="sm:w-1/2 mx-auto" alt="coding dev illustration">
</div>

## 技术栈

这个主题是用原版JavaScript（+ TypeScript用于类型检查）和一些ReactJS用于某些交互编写的。顺风CSS用于样式;和 Markdown 用于博客内容。

## 特点 

以下是本网站的某些功能。

- 完全响应和可访问
- 对搜索引擎优化友好
- 浅色和深色模式
- 模糊搜索
- 相应超快
- 可以写草稿帖子
- 分页
- 网站地图和RSS Feed
- 高度可定制

如果喜欢这个主题，可以为[该项目](https://github.com/satnaing/astro-paper)加星、贡献。
甚至可以通过作者的[email](mailto:contact@satnaing.dev)提供任何反馈。
